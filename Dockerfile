FROM thecodingmachine/nodejs:12 AS build

WORKDIR /app
USER root

COPY package.json yarn.lock ./
RUN yarn

COPY . .
RUN yarn build

FROM nginx:1.17-alpine

WORKDIR /app

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /app/web ./web
