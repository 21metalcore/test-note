import Vue from 'vue'
import Vuex from 'vuex'
import notesList from './modules/notesList'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    notesList
  }
})
