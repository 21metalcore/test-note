import Cookies from 'js-cookie'

const notesList = {
  state: {
    notes: JSON.parse(Cookies.get('notes') ?? '[]'), //получаем записи о заметках из куки, если их нет, то оставляем пустой массив
    lastID: JSON.parse(Cookies.get('lastID') ?? 0) //тоже самое, только с последним айди
  },
  mutations: {
    //добавление записки
    ADD_NOTE: (state, note) => {
      state.lastID += 1 //инкрементируем айди
      note.id = state.lastID //присваеваем его к новой записке
      Cookies.set('lastID', state.lastID) //добавляем в куки последий айди
      state.notes.push(note) //добавляем новую записку
      Cookies.set('notes', state.notes) //добавляем инфу в куки
    },
    //удаление записки
    DELETE_NOTE: (state, id) => {
      state.notes = state.notes.filter(x => x.id != id) //фильтруем записки по айди исключая удаляемую
      Cookies.set('notes', state.notes) //обновляем куки
    },
    //обновление существующей записки
    UPDATE_NOTE: (state, note) => {
      Object.assign(state.notes[state.notes.findIndex(x => x.id == note.id)], note) //поиск по айди и обновление записки
    }
  },
  getters: {
    //геттер всех записок
    getNotes: state => {
      return state.notes
    },
    //геттер записки по айди
    getNoteByID: state => index => {
      return state.notes.find(x => x.id == index)
    }
  }
}

export default notesList
